class CommentsController < ApplicationController

	http_basic_authenticate_with name: "Tyrone", password: "demo", except: [:destroy]
	
	def create
		# Find parent article based on foreign key param
		@article = Article.find params[:article_id]
		#  Create comment associated with parent article
		@comment = @article.comments.create(comment_params)
		redirect_to article_path(@article)
		
	end

	def destroy
		@article = Article.find params[:article_id]
		@comment = @article.comments.find(params[:id])
		@comment.destroy
		redirect_to article_path(@article)
	end

	private
    def comment_params
      params.require(:comment).permit(:commenter, :body)
    end
end
