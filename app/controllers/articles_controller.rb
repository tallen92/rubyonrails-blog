class ArticlesController < ApplicationController
public
	#layout 'standard' #Use standard file layout for views under this controller

	http_basic_authenticate_with name: "Tyrone", password: "demo", except: [:index,:show]
	before_action :find_project, only: [:show, :edit, :update, :destroy]
# rails route HTTP request and URI determine action
	def index
		#List all
		@articles = Article.all
	end

	def show
		#All instance variables are passed to view
		#id passed in
		# @article = Article.find(params[:id])
	end

	def new
	  @article = Article.new
	end

	def edit
		# @article = Article.find(params[:id])
		
	end

	def create
		@article = Article.new(article_params)

		if  @article.save
			redirect_to @article
		else 
			render 'new'
		end
	end

	def update
		# @article = Article.find(params[:id])

		if  @article.update(article_params)
			# routes to show method
			redirect_to @article
		else 
			render 'edit'
		end
	end

	def destroy
		# @article = Article.find params[:id]
		@article.destroy

		redirect_to articles_path
	end

private
	def article_params
		#Strong parameters to create whitelist of allowed params
		params.require(:article).permit(:title, :text, :attachment)
	end
end

def find_project
	@article = Article.find(params[:id])
end
